/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TOXER_INTERFACE_TOX_NOTIFY_H
#define TOXER_INTERFACE_TOX_NOTIFY_H

#include "ToxTypes.h"

class QByteArray;
class QString;

class IToxFileNotifier {
public:
  IToxFileNotifier() = default;
  virtual ~IToxFileNotifier() = default;
  virtual void on_avatar_request(quint32 friend_no, quint32 file_no, quint64 file_size) = 0;
  virtual void on_cancelled(quint32 friend_no, quint32 file_no) = 0;
  virtual void on_chunk(quint32 friend_no, quint32 file_no, quint64 file_pos, const char* data, quint64 chunk_size) = 0;
  virtual void on_file_request(quint32 friend_no, quint32 file_no, const QString& file_name, quint64 file_size) = 0;
  virtual void on_file_transfer(quint32 friend_no, quint32 file_no, const QString& path) = 0;
  virtual void on_finished(quint32 friend_no, quint32 file_no) = 0;
  virtual void on_next_chunk(quint32 friend_no, quint32 file_no, quint64 pos, quint64 len) = 0;
  virtual void on_paused(quint32 friend_no, quint32 file_no) = 0;
  virtual void on_resumed(quint32 friend_no, quint32 file_no) = 0;
};

class IToxFriendNotifier {
protected:
  IToxFriendNotifier();
  virtual ~IToxFriendNotifier();

public:
  virtual void on_name_changed(quint32 friend_no, const QString& name) = 0;
  virtual void on_status_message_changed(quint32 friend_no, const QString& message) = 0;
  virtual void on_status_changed(quint32 friend_no, ToxTypes::UserStatus status) = 0;
  virtual void on_is_online_changed(quint32 friend_no, bool online) = 0;
  virtual void on_is_typing_changed(quint32 friend_no, bool typing) = 0;
  virtual void on_message(quint32 friend_no, const QString& message) = 0;
};

class IToxFriendsNotifier {
protected:
  IToxFriendsNotifier();
  virtual ~IToxFriendsNotifier();

public:
  virtual void on_added(quint32 index) = 0;
  virtual void on_deleted(quint32 index) = 0;
};

class IToxProfileNotifier {
protected:
  IToxProfileNotifier();
  virtual ~IToxProfileNotifier();

public:
  virtual void on_user_name_changed(const QString& userName) = 0;
  virtual void on_is_online_changed(bool online) = 0;
  virtual void on_status_message_changed(const QString& message) = 0;
  virtual void on_status_changed(ToxTypes::UserStatus status) = 0;
  virtual void on_friend_request(const QByteArray& pk, const QString& message) = 0;
};

#endif
