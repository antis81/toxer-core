/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2019-2023 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ToxerRandom.h"

#if QT_VERSION < QT_VERSION_CHECK(5,10,0)
#include <ctime>
#else
#include <QRandomGenerator>
#endif

/** @class ToxerRandomValues
@brief A QVector<quint32> constant sequence of bounded random numbers.

The internal random number generator will be seeded and constructs a different
number sequence for the instance.
*/

/** @brief Contstructs initialized instance with random number sequence.
@param count  the count of numbers to generate
@param highest  the highest possible number (exclusive => highest - 1)
@return QVector of generated random numbers.
*/
ToxerRandomValues::ToxerRandomValues(int count, quint32 highest)
{
  Q_ASSERT(highest > 1);
#if QT_VERSION < QT_VERSION_CHECK(5,10,0)
  // seed randomizer with current time
  std::srand(static_cast<quint32>(std::time(nullptr)));
#else
  auto rg = QRandomGenerator::securelySeeded();
#endif

  while (QList::size() < count) {
    // no duplicates
    quint32 v;
    do {
#if QT_VERSION < QT_VERSION_CHECK(5,10,0)
      v = static_cast<quint32>(qrand()) % (highest - 1);
#else
      v = rg.bounded(highest);
#endif
    } while(contains(v));

    // append the unique node
    append(v);
  }
  Q_ASSERT(QList::size() == count);
}
