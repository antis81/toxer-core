# Toxer: The Tox API That Fits Qt(Quick)

Toxer provides a quality and simple to use Tox API for developers who want to build Qt/QML applications around the Tox protocol.

## Disclaimer

![! WARNING: EXPERIMENTAL !](webres/experimental.png  "ToxIsExperimental")

The Tox protocol is marked "experimental" in sense of IT-Security.
This implies to the open-source Toxer API which is built with focus on personal research.
The Toxer project does not hold responsible for possible security issues.

Use at own risk!

Visit [toktok/c-toxcore on Github](https://github.com/toktok/c-toxcore) for more detailed information on the topic.

## Toxer UI Integrations

The following UI integrations are being actively developed and maintained:

- Desktop: https://gitlab.com/Toxer/toxer-desktop
- Sailfish: https://gitlab.com/Toxer/toxer-sfos

## Online Resources

- Please file issues and feature requests for **Toxer** on [gitlab](https://gitlab.com/Toxer/toxer-core).
- Also you are welcome to [contribute](CONTRIBUTE.md) to the Toxer project.
- For discussions concerning the **Tox protocol** find the Tox enthusiasts on IRC at [#tox-dev](irc://irc.freenode.net/#tox-dev) and [#toktok](irc://irc.freenode.net/#toktok).

## Usage

Integrating the Toxer API in your own project is fairly simple. Toxer provides a set of QML items to query and set Tox values.
For example `ToxProfileQuery` and `ToxFriends` allow to perform common tasks like showing each Tox-Friend's name, online status, etc.

Example:

```qml
import QtQuick 2.0

import toxer.qml 1.0

MyItem {
    ToxFriends {
        id: tfq
        onMessage: {
            console.log("Friend " + tfq.name(index) + " says: " + message);
        }
    }

    Component.onCompleted {
        // notes:
        // 1. For the record: The password used here is an example.
        //    It is required to decrypt the Tox profile locally(!) on your device.
        //    Toxer respects your privacy and does in no way save your password.
        //    You can input the password in a PasswordField or similar text field!
        // 2. You can either create an empty profile or open an existing one.
        //    For the example we assume an existing profile "my_profile.tox" on disk.
        Toxer.activateProfile("my_profile.tox", "my_secret_password");
        console.log("I have as many as " + tfq.count + " friends on Tox. Yay!");
    }
}
```

## Project Goals

- focus on lean use of system resources
- focus on code stability
- runs on a variety of devices
- fun & easy to use QtQuick API (Integration with QWidgets is untested!)

## Implemented Features

- add/remove Tox-Friends
- Tox-Address validation (primarily used for adding friends)
- 1v1 chat (note: Messages are not being locally stored yet!)
- Tox-Self (profile) en-/decryption
- support for multi-profile creation/selection

## License

The Toxer API is freely usable under the terms of the MIT license.

Note that third-party software used by Toxer is licensed separately.
For example Qt, toxcore and sodium each come with their own license that applies to those components.
